/**
Copyright (C) 2011  Augusto Recordon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package ar.com.nasel.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author Augusto Recordon
 */
public class FileWriter {
    
    /* *************************************************************************************************** */

    protected String filename;
    
    /* *************************************************************************************************** */
    
    private File file;

    /* *************************************************************************************************** */
    
    private BufferedWriter bw;

    /* *************************************************************************************************** */
    
    public FileWriter(String filename) {
        super();
        this.filename = filename;
    }

    /* *************************************************************************************************** */
    
    public void create() throws IOException {
        this.file = new File(this.filename);
        this.bw = new BufferedWriter(new java.io.FileWriter(this.file));
    }

    /* *************************************************************************************************** */
    
    public void write(String string) throws IOException {
        this.bw.write(string);
    }

    /* *************************************************************************************************** */
    
    public void write(char[] chars) throws IOException {
        this.bw.write(chars);
    }

    /* *************************************************************************************************** */
    
    public void write(char[] chars, int i, int j) throws IOException {
        this.bw.write(chars,i,j);
    }    
    
    /* *************************************************************************************************** */
    
    public void write(String string,int i, int j) throws IOException {
        this.bw.write(string,i,j);
    }
    
    /* *************************************************************************************************** */
    
    public void writeln(String string) throws IOException {
        this.write(string);
        this.bw.newLine();
    }

    /* *************************************************************************************************** */
    
    public void close() throws IOException {
        this.bw.close();
        this.file = null;
    }
    
    /* *************************************************************************************************** */
}
