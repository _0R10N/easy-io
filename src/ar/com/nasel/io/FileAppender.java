/**
Copyright (C) 2011  Augusto Recordon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package ar.com.nasel.io;

import ar.com.nasel.io.tool.TextTool;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author Augusto Recordon
 * 
 * This class provides a mechanism to append data to an existing file.
 */
public class FileAppender extends FileWriter {
    
    /* *************************************************************************************************** */

    protected RandomAccessFile raf;

    /* *************************************************************************************************** */
    
    /**
     * Constructor that receives the full-qualified name of the file.
     * 
     * @param filename 
     */
    public FileAppender(String filename) {
        super(filename);
    }

    /* *************************************************************************************************** */
    
    /**
     * This method will open the specified file.
     * 
     * @throws IOException 
     */
    @Override
    public void create() throws IOException {
        this.raf = new RandomAccessFile(this.filename, TextTool.READ_WRITE_ACCESS);
        this.raf.seek(this.raf.length());
    }

    /* *************************************************************************************************** */
    
    /**
     * This method writes the given string into the file.
     * 
     * @param string
     * @throws IOException 
     */
    @Override
    public void write(String string) throws IOException {
        this.raf.writeChars(string);
    }

    /* *************************************************************************************************** */
    
    /**
     * This method writes the string given as argument to the file, and then inserts a 
     * line break.
     * 
     * @param string
     * @throws IOException 
     */
    @Override
    public void writeln(String string) throws IOException {
        this.write(string);
        this.write(TextTool.LINE_BREAK);
    }

    /* *************************************************************************************************** */
    
    /**
     * This method will close the file and release any resource related to it.
     * 
     * @throws IOException 
     */
    @Override
    public void close() throws IOException {
        this.raf.close();
    }
    
    /* *************************************************************************************************** */
    
}
