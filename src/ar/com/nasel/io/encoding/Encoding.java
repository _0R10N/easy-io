/**
Copyright (C) 2011  Augusto Recordon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package ar.com.nasel.io.encoding;

import java.nio.charset.Charset;

/**
 *
 * @author Augusto Recordon
 * 
 * This class contains functionality related to charset and encoding handling.
 */
public class Encoding {
    
    /* *************************************************************************************************** */
    
    /**
     *  UTF-8 encoding constant.
     */
    public final static String UTF8 = "UTF-8";
    
    /* *************************************************************************************************** */
    
    /**
     * Default encoding is UTF-8. You cannot change this default value, but you will be able to work with
     * any charset you want, specified on this class or not.
     * @see Charset
     * 
     */
    public final static String DEFAULT_ENCODING = Encoding.UTF8;    
    
    /* *************************************************************************************************** */
   
    /**
     * This method returns the charset corresponding to the encoding received.
     * 
     * @param encoding
     * @return 
     */
    public static Charset getCharset(String encoding){
        return Charset.forName(encoding);
    }
    
    /* *************************************************************************************************** */
    
    /**
     * When calling to this method, no encoding is specified, thus the default is assumed.
     * 
     * @return 
     */
    public static Charset getCharset(){
        return Charset.forName(Encoding.DEFAULT_ENCODING);
    }
    
    /* *************************************************************************************************** */
}
