/**
Copyright (C) 2011  Augusto Recordon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package ar.com.nasel.io.tool;

/**
 *
 * @author Augusto Recordon
 * 
 * This class provides a set of constant and method that may come in handy when
 * working with files.
 */
public class TextTool {
    
    /* *************************************************************************************************** */
    
    /**
     * Default line-break char.
     */
    public final static String LINE_BREAK = "\n";
    
    /* *************************************************************************************************** */
    
    /**
     * String corresponding to file read-write access.
     */
    public final static String READ_WRITE_ACCESS = "wr";
    
    /* *************************************************************************************************** */
    
}
