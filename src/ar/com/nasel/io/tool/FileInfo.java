/**
Copyright (C) 2011  Augusto Recordon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package ar.com.nasel.io.tool;

import java.io.File;

/**
 *
 * @author Augusto Recordon
 * 
 * This class provides a set of methods to obtain information about files.
 */
public class FileInfo {

    /* *************************************************************************************************** */
    
    /**
     * This method evaluates whether a specific file exists in the filesystem.
     * 
     * @param fileName Full-qualified file name.
     * @return whether the file already exists.
     */
    public static boolean exists(String fileName) {
        File f = new File(fileName);
        return f.exists();
    }
    
    /* *************************************************************************************************** */
}
