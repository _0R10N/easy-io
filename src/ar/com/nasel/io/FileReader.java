/**
Copyright (C) 2012  Augusto Recordon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package ar.com.nasel.io;

import ar.com.nasel.io.encoding.Encoding;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

/**
 *
 * @author Augusto Recordon
 * 
 * This is a class for accessing files.
 * 
 */
public class FileReader {

    /* *************************************************************************************************** */
    
    private String fileName;
    
    /* *************************************************************************************************** */
    
    private FileInputStream fis;
    
    /* *************************************************************************************************** */
    
    private DataInputStream dis;
    
    /* *************************************************************************************************** */
    
    private BufferedReader br;
    
    /* *************************************************************************************************** */
    
    private boolean open;

    /* *************************************************************************************************** */

    private String encoding;
    
    /* *************************************************************************************************** */
    
    private boolean end;

    /* *************************************************************************************************** */
    /**
     * When using this constructor, default encoding will be used.
     * 
     * @param fileName 
     */
    public FileReader(String fileName) {
        this(fileName, Encoding.DEFAULT_ENCODING);
    }

    /* *************************************************************************************************** */
    /**
     * 
     * @param fileName
     * @param encoding 
     */
    public FileReader(String fileName, String encoding) {
        super();
        this.fileName = fileName;
        this.encoding = encoding;
    }

    /* *************************************************************************************************** */
    
    /**
     * This method evaluates whether the end of the file has been reached or not.
     * 
     * @return  end of file.
     */
    public boolean next() {
        return !this.end;
    }

    /* *************************************************************************************************** */
    
    /**
     * This method tries to read a line from the specified file.
     * 
     * @return
     * @throws IOException 
     */
    public String readln() throws IOException {
        String line = null;
        if (!this.end) {
            line = this.br.readLine();
            if (line == null) {
                this.end = true;
            }
        }

        return line;
    }

    /* *************************************************************************************************** */
    
    /**
     * This method tries to load the next portion of the file into the CharBuffer given.
     * 
     * @param cb
     * @return
     * @throws IOException 
     */
    public int read(CharBuffer cb) throws IOException {
        int res = -1;
        if (this.end) {
            res = this.br.read(cb);
            if (res == -1) {
                this.end = true;
            }
        }

        return res;
    }

    /* *************************************************************************************************** */
    
    /**
     * This method tries to fill the char array received as argument with the next sequence of chars
     * available in the file.
     * 
     * @param chars
     * @return
     * @throws IOException 
     */
    public int read(char[] chars) throws IOException {
        int res = -1;
        if (this.end) {
            res = this.br.read(chars);
            if (res == -1) {
                this.end = true;
            }
        }
        return -1;
    }

    /* *************************************************************************************************** */
    
    /**
     * This method will try to fill the section of the char array received specified by i and j, with the next
     * available characters in the file.
     * 
     * @param chars
     * @param i
     * @param j
     * @return
     * @throws IOException 
     */
    public int read(char[] chars, int i, int j) throws IOException {
        if (this.end) {
            return this.br.read(chars, i, j);
        }
        return -1;
    }
    /* *************************************************************************************************** */

    /**
     * This method perform some basic initialization and the opening of the file.
     * 
     * @throws FileNotFoundException 
     */
    public void open() throws FileNotFoundException {
        this.fis = new FileInputStream(this.fileName);
        this.dis = new DataInputStream(this.fis);
        this.br = new BufferedReader(new InputStreamReader(this.dis, Charset.forName(this.encoding)));
        this.open = true;
    }
    
    /* *************************************************************************************************** */
    
    /**
     * This method will close the file (if open) and release all of the resources.
     * @throws IOException 
     * 
     */
    public void close() throws IOException {
        if (this.open) {
            this.dis.close();
            this.br.close();
            this.open = false;
            this.end = true;
        }
        
    }
    
    
    /* *************************************************************************************************** */
    /**
     * This method resets the reading position to the beggining of the file.
     * 
     * @throws IOException 
     */
    public void reset() throws IOException {
        this.close();
        this.open();
    }
    
    /* *************************************************************************************************** */
    
    /**
     * This method checks whether the file has already been closed or not.
     * @return 
     */
     public boolean isOpen() {
        return this.open;
    }
     
     /* *************************************************************************************************** */
}
