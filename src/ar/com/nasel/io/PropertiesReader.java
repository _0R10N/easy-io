/**
Copyright (C) 2011  Augusto Recordon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package ar.com.nasel.io;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Properties;
import java.util.Set;

/**
 *
 * @author Augusto Recordon
 */
public class PropertiesReader {
    
    protected String fileName;
    
    protected Properties propertiesFile;
    
    public PropertiesReader(String fileName){
        super();
        this.fileName = fileName;
        
    }
    
    
    public void load() throws IOException{
        this.propertiesFile = new Properties();
        this.propertiesFile.load(new FileInputStream(this.fileName));
    }
    
    public String get(String property){
        return this.propertiesFile.getProperty(property);
    }
    
    public Set keySet(){
        return this.propertiesFile.keySet();
    }
    
    public Collection values(){
        return this.propertiesFile.values();
    }
}
