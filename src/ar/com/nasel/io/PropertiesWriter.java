/**
Copyright (C) 2011  Augusto Recordon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package ar.com.nasel.io;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Properties;

/**
 *
 * @author Augusto Recordon
 */
public class PropertiesWriter {
    
    protected String fileName;
    
    private Properties propertiesFile;
    
            
    public PropertiesWriter(String fileName){
        super();
        this.fileName = fileName;
    }
    
    public void set(String property, String value){
        this.propertiesFile.setProperty(property, value);
    }
    
    public void store() throws IOException{
        this.store(new FileOutputStream(this.fileName),null);
    }
    
    public void store(FileOutputStream fos) throws IOException{
        this.store(fos,null);
    }
    
    public void store(FileOutputStream fos, String comments) throws IOException{
        this.propertiesFile.store(fos,comments);
    }
    
    public void store(Writer writer, String comments) throws IOException{
        this.propertiesFile.store(writer,comments);
    }

    
    public void storeToXML(OutputStream os, String comments) throws IOException{
        this.propertiesFile.storeToXML(os,comments);
    }
    
    
    public void storeToXML(OutputStream os, String comments, String string2) throws IOException{
        this.propertiesFile.storeToXML(os,comments,string2);
    }
    
    public void clear(){
        this.propertiesFile.clear();
    }
}
